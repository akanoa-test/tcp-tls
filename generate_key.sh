#!/usr/bin/env bash
pushd assets/certs || exit
openssl req \
        -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes \
        -subj "/C=FR/ST=France/L=Paris/O=NoaCorp/CN=lab.noa.eu"
openssl pkcs12 -export -out key.p12 -inkey key.pem -in cert.pem -passout pass:toto
popd || exit