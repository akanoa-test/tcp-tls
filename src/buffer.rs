use std::io::Read;
use std::ops::{Deref, DerefMut};

pub struct Buffer {
    data: Box<[u8]>,
}

impl Buffer {
    pub fn new(capacity: usize) -> Self {
        Self {
            data: vec![0; capacity].into_boxed_slice(),
        }
    }
}

impl Deref for Buffer {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl DerefMut for Buffer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}

impl Read for Buffer {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        self.data.copy_from_slice(&buf[0..self.data.len()]);
        Ok(self.data.len())
    }
}
