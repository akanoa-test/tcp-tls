use crate::data::header::TlsRecordHeader;

pub mod header;
pub mod hello;

enum Payload {
    Hello(hello::HelloHandshake),
}

struct TlsRecord {
    header: TlsRecordHeader,
    payload: Payload,
}
