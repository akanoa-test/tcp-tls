use deku::{DekuContainerRead, DekuRead};
use eyre::{eyre, OptionExt};
use strum::FromRepr;

#[derive(FromRepr, Debug)]
#[repr(u8)]
enum ContentType {
    Handshake = 0x16,
}

#[derive(FromRepr, Debug)]
#[repr(u16)]
enum LegacyVersion {
    TlsV1_0 = 0x301,
}

#[derive(Debug)]
pub struct TlsRecordHeader {
    content_type: ContentType,
    legacy_version: LegacyVersion,
    pub(crate) length: usize,
}

#[derive(DekuRead, Debug)]
#[deku(endian = "big")]
struct RawRecordHeader {
    content_type: u8,
    legacy_version: u16,
    length: u16,
}

impl TryFrom<RawRecordHeader> for TlsRecordHeader {
    type Error = eyre::Report;

    fn try_from(raw_header: RawRecordHeader) -> Result<Self, Self::Error> {
        Ok(TlsRecordHeader {
            content_type: ContentType::from_repr(raw_header.content_type)
                .ok_or_eyre(eyre!("Unknown content type {:x}", raw_header.content_type))?,
            legacy_version: LegacyVersion::from_repr(raw_header.legacy_version).ok_or_eyre(
                eyre!("Unknown legacy version {:x}", raw_header.legacy_version),
            )?,
            length: raw_header.length as usize,
        })
    }
}

impl TryFrom<&[u8]> for TlsRecordHeader {
    type Error = eyre::Report;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let ((_remain, _remain_size), raw_header) = RawRecordHeader::from_bytes((value, 0))?;
        raw_header.try_into()
    }
}
