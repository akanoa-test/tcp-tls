use crate::dbg_hex;
use deku::{DekuContainerRead, DekuRead};
use strum::FromRepr;

#[derive(FromRepr, Debug)]
#[allow(non_camel_case_types)]
#[repr(u16)]
enum SupportedVersion {
    TLS_1_3 = 0x0304,
    TLS_1_2 = 0x0303,
    TLS_1_1 = 0x0302,
    TLS_1_0 = 0x0301,
}

#[derive(DekuRead, Debug)]
#[deku(endian = "big")]
struct RawSupportedVersionExtension {
    length: u8,
    #[deku(count = "length / 2")]
    versions_data: Vec<u16>,
}

#[derive(Debug)]
pub(in crate::data::hello) struct SupportedVersionExtension {
    versions: Vec<SupportedVersion>,
}

impl TryFrom<&[u8]> for SupportedVersionExtension {
    type Error = eyre::Report;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let (_, raw_extension) = RawSupportedVersionExtension::from_bytes((value, 0))?;
        let versions = raw_extension
            .versions_data
            .into_iter()
            .flat_map(SupportedVersion::from_repr)
            .collect();
        Ok(SupportedVersionExtension { versions })
    }
}
