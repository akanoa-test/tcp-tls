use crate::dbg_hex;
use deku::{DekuContainerRead, DekuRead};

#[derive(DekuRead, Debug)]
#[deku(endian = "big")]
struct RawSessionTicketExtension {
    length: u16,
    #[deku(count = "length")]
    data: Vec<u8>,
}

#[derive(Debug)]
pub(in crate::data::hello) struct SessionTicketExtension {
    session_ticket: Option<Vec<u8>>,
}

impl TryFrom<&[u8]> for SessionTicketExtension {
    type Error = eyre::Report;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        if value.is_empty() {
            return Ok(SessionTicketExtension {
                session_ticket: None,
            });
        }

        let (_, raw_extension) = RawSessionTicketExtension::from_bytes((value, 0))?;
        let session_ticket = if !raw_extension.data.is_empty() {
            Some(raw_extension.data)
        } else {
            None
        };
        Ok(SessionTicketExtension { session_ticket })
    }
}
