use deku::{DekuContainerRead, DekuRead};
use strum::FromRepr;

#[allow(non_camel_case_types)]
#[derive(FromRepr, Debug)]
#[repr(u16)]
enum SignatureAlgorithms {
    ecdsa_secp256r1_sha256 = 0x0403,
    ecdsa_secp384r1_sha384 = 0x0503,
    ecdsa_secp521r1_sha512 = 0x0603,
    ed25519 = 0x0807,
    ed448 = 0x0808,
    rsa_pss_pss_sha256 = 0x0809,
    rsa_pss_pss_sha384 = 0x080a,
    rsa_pss_pss_sha512 = 0x080b,
    rsa_pss_rsae_sha256 = 0x0804,
    rsa_pss_rsae_sha384 = 0x0805,
    rsa_pss_rsae_sha512 = 0x0806,
    rsa_pkcs1_sha256 = 0x0401,
    rsa_pkcs1_sha384 = 0x0501,
    rsa_pkcs1_sha512 = 0x0601,
    SHA224_ECDSA = 0x0303,
    ecdsa_sha1 = 0x0203,
    SHA224_RSA = 0x0301,
    rsa_pkcs1_sha1 = 0x0201,
    SHA224_DSA = 0x0302,
    SHA1_DSA = 0x0202,
    SHA256_DSA = 0x0402,
    SHA384_DSA = 0x0502,
    SHA512_DSA = 0x0602,
}

#[derive(DekuRead, Debug)]
#[deku(endian = "big")]
struct RawSignatureAlgorithmsExtension {
    length: u16,
    #[deku(count = "length / 2")]
    signature_algorithms: Vec<u16>,
}

#[derive(Debug)]
pub(in crate::data::hello) struct SignatureAlgorithmsExtension {
    algorithms: Vec<SignatureAlgorithms>,
}

impl TryFrom<&[u8]> for SignatureAlgorithmsExtension {
    type Error = eyre::Report;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let (_, raw_extension) = RawSignatureAlgorithmsExtension::from_bytes((value, 0))?;
        let algorithms = raw_extension
            .signature_algorithms
            .into_iter()
            .flat_map(SignatureAlgorithms::from_repr)
            .collect();
        Ok(SignatureAlgorithmsExtension { algorithms })
    }
}
