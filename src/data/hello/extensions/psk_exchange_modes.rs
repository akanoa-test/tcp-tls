use deku::{DekuContainerRead, DekuRead};
use strum::FromRepr;

#[derive(FromRepr, Debug)]
#[allow(non_camel_case_types)]
#[repr(u8)]
enum PskExchangesModes {
    PSK_ECDH_KeyExchange = 0x1,
}

#[derive(DekuRead, Debug)]
struct RawPskExchangesModesExtension {
    length: u8,
    #[deku(count = "length")]
    data: Vec<u8>,
}

#[derive(Debug)]
pub(in crate::data::hello) struct PskExchangesModesExtension {
    modes: Vec<PskExchangesModes>,
}

impl TryFrom<&[u8]> for PskExchangesModesExtension {
    type Error = eyre::Report;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let (_, raw_extension) = RawPskExchangesModesExtension::from_bytes((value, 0))?;
        let modes = raw_extension
            .data
            .into_iter()
            .flat_map(PskExchangesModes::from_repr)
            .collect();
        Ok(PskExchangesModesExtension { modes })
    }
}
