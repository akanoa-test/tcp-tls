use crate::dbg_hex;
use deku::{DekuContainerRead, DekuRead};
use strum::FromRepr;

#[derive(FromRepr, Debug)]
#[allow(non_camel_case_types)]
#[repr(u16)]
enum SupportedGroups {
    x25519 = 0x001d,
    secp256r1 = 0x0017,
    x448 = 0x001e,
    secp521r1 = 0x0019,
    secp384r1 = 0x0018,
}

#[derive(DekuRead, Debug)]
#[deku(endian = "big")]
struct RawSupportedGroupsExtension {
    list_length: u16,
    #[deku(count = "list_length / 2")]
    groups_data: Vec<u16>,
}

#[derive(Debug)]
pub(in crate::data::hello) struct SupportedGroupsExtrension {
    groups: Vec<SupportedGroups>,
}

impl TryFrom<&[u8]> for SupportedGroupsExtrension {
    type Error = eyre::Report;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let (_, raw_extension) = RawSupportedGroupsExtension::from_bytes((value, 0))?;

        let groups = raw_extension
            .groups_data
            .into_iter()
            .flat_map(SupportedGroups::from_repr)
            .collect();
        Ok(SupportedGroupsExtrension { groups })
    }
}
