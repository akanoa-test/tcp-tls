use crate::data::hello::Extension;
use deku::{DekuContainerRead, DekuRead};
use eyre::eyre;
use strum::FromRepr;

#[derive(DekuRead)]
#[deku(endian = "big")]
struct RawServerNameItem {
    pub(crate) server_name_type: u8,
    server_name_length: u16,
    #[deku(count = "server_name_length")]
    pub(crate) server_name_data: Vec<u8>,
}

#[derive(FromRepr)]
#[repr(u8)]
enum ServerNameType {
    HostName = 0,
}

#[derive(Debug)]
enum ServerName {
    HostName(String),
}

impl ServerName {
    pub(crate) fn from_bytes(kind: ServerNameType, data: Vec<u8>) -> eyre::Result<ServerName> {
        match kind {
            ServerNameType::HostName => Ok(ServerName::HostName(String::from_utf8(data)?)),
        }
    }
}

#[derive(Debug)]
pub(in crate::data::hello) struct ServerNameExtension {
    pub(crate) server_name_list: Vec<ServerName>,
}

impl TryFrom<&[u8]> for ServerNameExtension {
    type Error = eyre::Report;

    fn try_from(data: &[u8]) -> Result<Self, Self::Error> {
        // Get the extension from bytes
        let (_remain, raw_extension_server_name) = RawServerNameExtension::from_bytes((data, 0))?;
        // Get extension data
        let extension_data = raw_extension_server_name.server_name_data;
        let mut cursor = 0;
        let mut server_name_list = vec![];
        // Get servers name
        loop {
            let ((_remain, remain_size), server_name_item) =
                RawServerNameItem::from_bytes((&extension_data, cursor * 8))?;

            cursor += remain_size;

            if let Some(server_name_type) =
                ServerNameType::from_repr(server_name_item.server_name_type)
            {
                let server_name =
                    ServerName::from_bytes(server_name_type, server_name_item.server_name_data)?;
                server_name_list.push(server_name)
            }

            if remain_size == 0 {
                break;
            }
        }

        Ok(ServerNameExtension { server_name_list })
    }
}

#[derive(DekuRead)]
#[deku(endian = "big")]
struct RawServerNameExtension {
    list_length: u16,
    #[deku(count = "list_length")]
    pub(crate) server_name_data: Vec<u8>,
}

#[allow(non_camel_case_types)]
#[derive(FromRepr, Debug)]
#[repr(u16)]
pub enum Cipher {
    TLS_AES_256_GCM_SHA384 = 0x1302,
    TLS_CHACHA20_POLY1305_SHA256 = 0x1303,
    TLS_AES_128_GCM_SHA256 = 0x1301,
    TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384 = 0xc02c,
    TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 = 0xc030,
    TLS_DHE_RSA_WITH_AES_256_GCM_SHA384 = 0x009f,
    TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256 = 0xcca9,
    TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256 = 0xcca8,
    TLS_DHE_RSA_WITH_CHACHA20_POLY1305_SHA256 = 0xccaa,
    TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 = 0xc02b,
    TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 = 0xc02f,
    TLS_DHE_RSA_WITH_AES_128_GCM_SHA256 = 0x009e,
    TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384 = 0xc024,
    TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384 = 0xc028,
    TLS_DHE_RSA_WITH_AES_256_CBC_SHA256 = 0x006b,
    TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 = 0xc023,
    TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256 = 0xc027,
    TLS_DHE_RSA_WITH_AES_128_CBC_SHA256 = 0x0067,
    TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA = 0xc00a,
    TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA = 0xc014,
    TLS_DHE_RSA_WITH_AES_256_CBC_SHA = 0x0039,
    TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA = 0xc009,
    TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA = 0xc013,
    TLS_DHE_RSA_WITH_AES_128_CBC_SHA = 0x0033,
    TLS_RSA_WITH_AES_256_GCM_SHA384 = 0x009d,
    TLS_RSA_WITH_AES_128_GCM_SHA256 = 0x009c,
    TLS_RSA_WITH_AES_256_CBC_SHA256 = 0x003d,
    TLS_RSA_WITH_AES_128_CBC_SHA256 = 0x003c,
    TLS_RSA_WITH_AES_256_CBC_SHA = 0x0035,
    TLS_RSA_WITH_AES_128_CBC_SHA = 0x002f,
    TLS_EMPTY_RENEGOTIATION_INFO_SCSV = 0x00ff,
}
