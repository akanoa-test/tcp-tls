use deku::{DekuContainerRead, DekuRead};
use strum::FromRepr;

#[derive(FromRepr, Debug)]
#[allow(non_camel_case_types)]
#[repr(u8)]
enum EllipticCurveFormat {
    UNCOMPRESSED = 0,
    ANSIX_962_COMPRESSED_PRIME = 1,
    ANSIX_962_COMPRESSED_CHAR2 = 2,
}

#[derive(DekuRead, Debug)]
struct RawEllipticCurveExtension {
    length: u8,
    #[deku(count = "length")]
    formats: Vec<u8>,
}

#[derive(Debug)]
pub(in crate::data::hello) struct EllipticCurveExtension {
    pub formats: Vec<EllipticCurveFormat>,
}

impl TryFrom<&[u8]> for EllipticCurveExtension {
    type Error = eyre::Report;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let (_, raw_extension) = RawEllipticCurveExtension::from_bytes((value, 0))?;
        let formats = raw_extension
            .formats
            .into_iter()
            .flat_map(EllipticCurveFormat::from_repr)
            .collect::<Vec<EllipticCurveFormat>>();
        Ok(EllipticCurveExtension { formats })
    }
}
