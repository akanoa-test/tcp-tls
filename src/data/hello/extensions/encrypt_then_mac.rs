use crate::dbg_hex;
use deku::{DekuContainerRead, DekuRead};

#[derive(Debug)]
pub(in crate::data::hello) struct EncryptThenMacExtension;

impl TryFrom<&[u8]> for EncryptThenMacExtension {
    type Error = eyre::Report;

    fn try_from(_value: &[u8]) -> Result<Self, Self::Error> {
        Ok(EncryptThenMacExtension)
    }
}
