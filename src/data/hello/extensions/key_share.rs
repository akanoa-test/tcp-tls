use crate::data::hello::bytes_to_hex;
use deku::{DekuContainerRead, DekuRead};
use eyre::eyre;
use std::fmt::{Debug, Formatter};
use strum::FromRepr;

#[derive(FromRepr, Debug)]
#[allow(non_camel_case_types)]
#[repr(u16)]
enum Group {
    x25519 = 0x1d,
}

#[derive(DekuRead, Debug)]
#[deku(endian = "big")]
struct RawKeyShareExtension {
    _length: u16,
    group: u16,
    key_exchange_length: u16,
    #[deku(count = "key_exchange_length")]
    key_exchange: Vec<u8>,
}

pub(in crate::data::hello) struct KeyShareExtension {
    group: Group,
    key_exchange: Vec<u8>,
}

impl Debug for KeyShareExtension {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("KeyShareExtension")
            .field("group", &self.group)
            .field("key_exchange", &bytes_to_hex(&self.key_exchange))
            .finish()
    }
}

impl TryFrom<&[u8]> for KeyShareExtension {
    type Error = eyre::Report;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let (_, raw_extension) = RawKeyShareExtension::from_bytes((value, 0))?;
        let group = Group::from_repr(raw_extension.group)
            .ok_or(eyre!("Unknown group for ID {}", raw_extension.group))?;
        Ok(KeyShareExtension {
            group,
            key_exchange: raw_extension.key_exchange,
        })
    }
}
