use crate::data::hello::extensions::elliptic_curve::EllipticCurveExtension;
use crate::data::hello::extensions::encrypt_then_mac::EncryptThenMacExtension;
use crate::data::hello::extensions::extended_master_secret::ExtendedMasterSecretExtension;
use crate::data::hello::extensions::key_share::KeyShareExtension;
use crate::data::hello::extensions::psk_exchange_modes::PskExchangesModesExtension;
use crate::data::hello::extensions::server_name::ServerNameExtension;
use crate::data::hello::extensions::session_ticket::SessionTicketExtension;
use crate::data::hello::extensions::signature_algorithms::SignatureAlgorithmsExtension;
use crate::data::hello::extensions::supported_groups::SupportedGroupsExtrension;
use crate::data::hello::extensions::supported_versions::SupportedVersionExtension;
use chrono::DateTime;
use deku::{DekuContainerRead, DekuRead};
use extensions::server_name::Cipher;
use strum::FromRepr;

mod extensions;

#[derive(FromRepr, Debug)]
#[repr(u16)]
enum ExtensionType {
    ServerName = 0,
    EllipticCurvePointFormat = 0x0b,
    SupportedGroups = 0x0a,
    SessionTicket = 0x23,
    EncryptThenMac = 0x16,
    ExtendedMasterSecret = 0x17,
    SignatureAlgorithms = 0x0d,
    SupportedVersions = 0x2b,
    PskExchangeModes = 0x2d,
    KeyShare = 0x33,
}

impl ExtensionType {
    fn try_to_extension(&self, data: &[u8]) -> eyre::Result<Extension> {
        match self {
            ExtensionType::ServerName => {
                Ok(Extension::ServerName(ServerNameExtension::try_from(data)?))
            }
            ExtensionType::EllipticCurvePointFormat => Ok(Extension::EllipticCurveFormats(
                EllipticCurveExtension::try_from(data)?,
            )),
            ExtensionType::SupportedGroups => Ok(Extension::SupportedGroups(
                SupportedGroupsExtrension::try_from(data)?,
            )),
            ExtensionType::SessionTicket => Ok(Extension::SessionTicket(
                SessionTicketExtension::try_from(data)?,
            )),
            ExtensionType::EncryptThenMac => Ok(Extension::EncryptThenMac(
                EncryptThenMacExtension::try_from(data)?,
            )),
            ExtensionType::ExtendedMasterSecret => Ok(Extension::ExtendedMasterSecret(
                ExtendedMasterSecretExtension::try_from(data)?,
            )),
            ExtensionType::SignatureAlgorithms => Ok(Extension::SignatureAlgorithms(
                SignatureAlgorithmsExtension::try_from(data)?,
            )),
            ExtensionType::SupportedVersions => Ok(Extension::SupportedVersion(
                SupportedVersionExtension::try_from(data)?,
            )),
            ExtensionType::PskExchangeModes => Ok(Extension::PskExchangeModes(
                PskExchangesModesExtension::try_from(data)?,
            )),
            ExtensionType::KeyShare => Ok(Extension::KeyShare(KeyShareExtension::try_from(data)?)),
        }
    }
}

#[derive(Debug)]
enum Extension {
    ServerName(ServerNameExtension),
    EllipticCurveFormats(EllipticCurveExtension),
    SupportedGroups(SupportedGroupsExtrension),
    SessionTicket(SessionTicketExtension),
    EncryptThenMac(EncryptThenMacExtension),
    ExtendedMasterSecret(ExtendedMasterSecretExtension),
    SignatureAlgorithms(SignatureAlgorithmsExtension),
    SupportedVersion(SupportedVersionExtension),
    PskExchangeModes(PskExchangesModesExtension),
    KeyShare(KeyShareExtension),
}

#[derive(DekuRead)]
#[deku(endian = "big")]
struct RawExtension {
    kind: u16,
    data_length: u16,
    #[deku(count = "data_length")]
    data: Vec<u8>,
}

#[derive(FromRepr, Debug)]
#[repr(u8)]
enum Compression {
    Null = 0,
}

#[derive(DekuRead, Debug)]
#[deku(endian = "big")]
struct RawHello {
    handshake_type: u8,
    #[deku(bits = "24")]
    length: u32,
    version: u16,
    random_epoch: u32,
    #[deku(count = "28")]
    random: Vec<u8>,
    session_id_length: u8,
    #[deku(count = "session_id_length")]
    session_id: Vec<u8>,
    ciphers_suite_length: u16,
    #[deku(count = "ciphers_suite_length / 2")]
    ciphers_suite: Vec<u16>,
    compression_length: u8,
    #[deku(count = "compression_length")]
    compression_methods: Vec<u8>,
    extensions_length: u16,
    #[deku(count = "extensions_length")]
    extension_payload: Vec<u8>,
}

#[derive(FromRepr, Debug)]
#[repr(u16)]
enum TlsVersion {
    TlsV1_2 = 0x303,
}

fn bytes_to_hex(data: &[u8]) -> String {
    data.iter().fold(String::new(), |mut acc, digit| {
        acc = format!("{acc}{digit:x}");
        acc
    })
}

fn payload_to_hello_client(data: &[u8]) -> eyre::Result<()> {
    let mut cursor = 0;
    let mut extensions = vec![];
    let mut previous_size = data.len();
    loop {
        let ((remain, _remain_size), extension) = RawExtension::from_bytes((data, cursor * 8))?;
        let delta = previous_size - remain.len();
        cursor += delta;
        if let Some(extension_type) = ExtensionType::from_repr(extension.kind) {
            let extension = ExtensionType::try_to_extension(&extension_type, &extension.data)?;
            dbg!(&extension);
            extensions.push(extension)
        }

        previous_size -= delta;
        if previous_size == 0 {
            break;
        }
    }
    Ok(())
}

#[derive(Debug)]
pub struct HelloHandshake {}

impl TryFrom<&[u8]> for HelloHandshake {
    type Error = eyre::Report;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let (_, raw_payload) = RawHello::from_bytes((value, 0))?;
        let random_str = bytes_to_hex(&raw_payload.random);
        let session_id = bytes_to_hex(&raw_payload.session_id);
        let random_time = DateTime::from_timestamp(raw_payload.random_epoch as i64, 0);
        // dbg!(&raw_payload);
        // dbg!(random_str);
        // dbg!(session_id);
        // dbg!(random_time);
        // dbg!(raw_payload.ciphers_suite_length);
        let cipher_suites = raw_payload
            .ciphers_suite
            .into_iter()
            .flat_map(Cipher::from_repr)
            .collect::<Vec<Cipher>>();
        let compression_methods = raw_payload
            .compression_methods
            .into_iter()
            .flat_map(Compression::from_repr)
            .collect::<Vec<Compression>>();
        dbg!(cipher_suites);
        dbg!(compression_methods);
        payload_to_hello_client(&raw_payload.extension_payload)?;

        Ok(HelloHandshake {})
    }
}
