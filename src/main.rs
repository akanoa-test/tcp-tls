use playground_tls::run;

fn main() {
    env_logger::init();

    if let Err(report) = run() {
        log::error!("{:?}", report)
    }
}
