use buffer::Buffer;
use data::header::TlsRecordHeader;
use native_tls::{TlsAcceptor, TlsStream};
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};
use std::sync::Arc;
use std::thread;
use std::time::Duration;

mod buffer;
mod data;

#[macro_export]
macro_rules! dbg_hex {
    ($x: expr) => {
        println!("{:#x?}", $x)
    };
}

fn handle_stream(mut stream: MaybeTls) -> eyre::Result<()> {
    log::debug!("Handle new connection");
    let response = b"hello";

    stream.write_all(response)?;
    stream.flush()?;

    Ok(())
}

fn mauvaise_idée(stream: &TcpStream) -> eyre::Result<()> {
    let mut buf = [0; 5];

    stream.set_read_timeout(Some(Duration::from_millis(1000)))?;

    stream.peek(&mut buf)?;

    let tls_record_header: TlsRecordHeader = (&buf[..]).try_into()?;

    dbg!(&tls_record_header);

    let payload_length = tls_record_header.length + 5;

    // hardcoded buffer
    let mut buf = Buffer::new(payload_length);

    let size = stream.peek(&mut buf)?;

    let hello = data::hello::HelloHandshake::try_from(&buf[5..size])?;
    dbg!(hello);

    Ok(())
}

fn maybe_tls(stream: TcpStream, acceptor: Arc<TlsAcceptor>) -> eyre::Result<MaybeTls> {
    match mauvaise_idée(&stream) {
        Ok(_) => {
            let stream = acceptor.accept(stream)?;
            Ok(MaybeTls::Tls(stream))
        }
        Err(_) => Ok(MaybeTls::Plain(stream)),
    }
}

enum MaybeTls {
    Tls(TlsStream<TcpStream>),
    Plain(TcpStream),
}

impl Read for MaybeTls {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        match self {
            MaybeTls::Tls(tls) => tls.read(buf),
            MaybeTls::Plain(plain) => plain.read(buf),
        }
    }
}

impl Write for MaybeTls {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        match self {
            MaybeTls::Tls(tls) => tls.write(buf),
            MaybeTls::Plain(plain) => plain.write(buf),
        }
    }

    fn flush(&mut self) -> std::io::Result<()> {
        match self {
            MaybeTls::Tls(tls) => tls.flush(),
            MaybeTls::Plain(plain) => plain.flush(),
        }
    }
}

pub fn run() -> eyre::Result<()> {
    let pkcs12_bytes = include_bytes!("../assets/certs/key.p12");
    let identity = native_tls::Identity::from_pkcs12(pkcs12_bytes, "toto")?;

    // create tls acceptor
    let acceptor = Arc::new(native_tls::TlsAcceptor::new(identity)?);

    // create tcp listener
    let connection_details = "127.0.0.1:8083";
    log::info!("Listening connection at {connection_details}");
    let listener = TcpListener::bind(connection_details)?;

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                log::info!("Accept new connection");
                let connection_acceptor = acceptor.clone();
                thread::spawn(move || -> eyre::Result<()> {
                    let stream = maybe_tls(stream, connection_acceptor.clone())?;
                    handle_stream(stream)?;
                    Ok(())
                });
            }
            Err(err) => {
                dbg!(err);
            }
        }
    }

    Ok(())
}
