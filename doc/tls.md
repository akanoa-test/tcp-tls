Create a PKCS12 a self-signed certificate on lolcat.host domain

```bash
pushd .. && bash generate_key.sh && popd 
```

Verify certificate

RSA

```bash
openssl rsa -in key.pem -check
```

PKCS12

```bash
openssl pkcs12 -info -in key.p12 -passin pass:toto
```